import unittest
from typing import Tuple
from pentac.application.mouse import MouseListener
from pynput import mouse
import time


class CallbackReceiver:

    def __init__(self):
        self.last_mouse_position: Tuple[int, int] = (0, 0)

    def callback(self, mouse_position: Tuple[int, int], *args, **kwargs):
        self.last_mouse_position = mouse_position

    def get_last_mouse_position(self):
        return self.last_mouse_position


class MouseTests(unittest.TestCase):

    def setUp(self):

        self.timeout_seconds = 1.0

        self.callback_receiver = CallbackReceiver()

        self.mouse_listener = MouseListener(time_after_mouse_stop_s=self.timeout_seconds,
                                            callback=self.callback_receiver.callback)
        self.mouse_listener.start_listening()

        self.mouse_controller = mouse.Controller()

    def tearDown(self) -> None:
        self.mouse_listener.stop()

    def _skip_if_manual_movement(self):
        if self.mouse_listener.get_mouse_movement_count() > 1:
            self.skipTest("Multiple mouse_listener events received - did you move the mouse_listener manually?")

    def test_change_position_1(self):
        self.mouse_controller.position = (5, 10)
        time.sleep(self.timeout_seconds + 0.1)

        self._skip_if_manual_movement()
        self.assertEqual((5, 10), self.callback_receiver.get_last_mouse_position())

    def test_change_position_2(self):
        self.mouse_controller.position = (501, 325)
        time.sleep(self.timeout_seconds + 0.1)

        self._skip_if_manual_movement()
        self.assertEqual((501, 325), self.callback_receiver.get_last_mouse_position())

    def test_change_position_3(self):
        self.mouse_controller.position = (1235, 0)
        time.sleep(self.timeout_seconds + 0.1)

        self._skip_if_manual_movement()
        self.assertEqual((1235, 0), self.callback_receiver.get_last_mouse_position())


if __name__ == '__main__':
    unittest.main()
