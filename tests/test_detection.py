import unittest
import pathlib
from pentac.application.detection import TextDetection


class DetectionTests(unittest.TestCase):

    def setUp(self):
        self.detector = TextDetection()
        self.images_path = pathlib.Path(__file__).parent.joinpath("test_images/")
        self.monitor_dpi = 94.11039611039611

    def test_image_001(self):
        detected_text = self.detector.get_text(self.images_path.joinpath("001_94.11039611039611dpi.jpg"),
                                               dpi=self.monitor_dpi)
        self.assertIn("284746", detected_text)

    def test_image_002(self):
        detected_text = self.detector.get_text(self.images_path.joinpath("002_94.11039611039611dpi.jpg"),
                                               dpi=self.monitor_dpi)
        self.assertIn("284746", detected_text)

    def test_image_003(self):
        detected_text = self.detector.get_text(self.images_path.joinpath("003_94.11039611039611dpi.jpg"),
                                               dpi=self.monitor_dpi)
        self.assertIn("284746", detected_text)
        self.assertIn("167124", detected_text)
        self.assertIn("98956", detected_text)

    def test_image_004(self):
        detected_text = self.detector.get_text(self.images_path.joinpath("004_94.11039611039611dpi.jpg"),
                                               dpi=self.monitor_dpi)
        self.assertIn("167124", detected_text)

    def test_image_005(self):
        detected_text = self.detector.get_text(self.images_path.joinpath("005_94.11039611039611dpi.jpg"),
                                               dpi=self.monitor_dpi)
        self.assertIn("167124", detected_text)

    def test_image_006(self):
        detected_text = self.detector.get_text(self.images_path.joinpath("006_94.11039611039611dpi.jpg"),
                                               dpi=self.monitor_dpi)
        self.assertNotIn("16/124", detected_text)


if __name__ == '__main__':
    unittest.main()
