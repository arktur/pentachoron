import pathlib
import threading
from typing import Tuple, List

from pentac.application.detection import TextDetection
from pentac.application.screenshot import ScreenShot
from pentac.application.mouse import MouseListener, MousePosition
from pentac.application.matcher import TranslationHandler
from pentac.configuration import ConfigurationLoader, PentacConfig
from pentac.configuration import Plugin as PluginConfig
from pentac.ui.application import Application
from pentac.ui.tray import TrayMenu

from pentac.ui.on_screen_display import OnScreenDisplay

from colorama import init
from termcolor import colored
init()


class DebugFlags:
    print_text_translation = False
    print_detection_profiling = False


class PentacApp:

    def __init__(self,
                 config_file: pathlib.Path = None,
                 config_file_template: pathlib.Path = None,
                 tesseract_executable_path: pathlib.Path = None,
                 auto_create_config_file: bool = True,
                 plugins: List[PluginConfig] = None,
                 additional_plugins: List[PluginConfig] = None,
                 time_after_mouse_stop_s: float = None,
                 theme_name: str = None,
                 theme_transparency_percent: float = None,
                 tray_color: str = None,
                 close_on_mouse_leave: bool = None,
                 follow_mouse_position: bool = None):

        self.config_loader = ConfigurationLoader()
        self.config_loader.load_from_file(config_file=config_file,
                                          config_file_template=config_file_template,
                                          auto_create=auto_create_config_file)
        self.config_loader.overwrite_settings(tesseract_executable_path=tesseract_executable_path,
                                              time_after_mouse_stop_s=time_after_mouse_stop_s,
                                              theme_name=theme_name,
                                              tray_color=tray_color,
                                              theme_transparency_percent=theme_transparency_percent,
                                              close_on_mouse_leave=close_on_mouse_leave,
                                              follow_mouse_position=follow_mouse_position)
        self.config_loader.overwrite_plugins(plugins=plugins)
        self.config_loader.add_plugins(additional_plugins=additional_plugins)
        self.config: PentacConfig = self.config_loader.get_configuration()

        self.application: Application = None

        self.text_detector = TextDetection(tesseract_executable_path=self.config.requirements.tesseract_executable_path)

        self.image_shooter: ScreenShot = None
        self.osd: OnScreenDisplay = None
        self.tray: TrayMenu = None

        self.mouse_listener = MouseListener(time_after_mouse_stop_s=self.config.behavior.time_after_mouse_stop_s,
                                            callback=self.mouse_stop_callback)
        self.mouse = MousePosition()

        self.translator = TranslationHandler(plugins_configuration=self.config.plugins)

    def execute(self):

        if self.config.requirements.tesseract_executable_path is None:
            print(colored("Requirement tesseract_executable_path was not found. "
                          "Try the following steps:", "blue", attrs=["bold", "underline"]))
            print(colored("1. Download and install tesseract from https://tesseract-ocr.github.io/tessdoc/Downloads "
                          "or install via your distribution", "blue"))
            print(colored(f"2. Add the executable to your PATH (if not done automatically) or configure the full path "
                          f"to the executable tesseract.exe inside your config file "
                          f"{self.config_loader.used_config_file}, instead of using auto detection.", "blue"))
            return

        self.application = Application()

        self.image_shooter = ScreenShot(100, 200, q_application=self.application.q_application)
        self.osd = OnScreenDisplay(q_application=self.application.q_application,
                                   theme_name=self.config.ui.theme_name,
                                   transparency_percent=self.config.ui.theme_transparency_percent,
                                   close_on_mouse_leave=self.config.ui.close_on_mouse_leave,
                                   follow_mouse_position=self.config.ui.follow_mouse_position,
                                   show_on_creation=False)
        self.osd.start()

        # Tray menu must be loaded afterwards, becuase it gets theming stuff from q_application.
        self.tray = TrayMenu(q_application=self.application.q_application)
        self.tray.add_configuration_info(config_loader=self.config_loader)
        self.tray.show(tray_color=self.config.tray.icon_color)

        self.mouse_listener.start_listening()

        self.application.execute()

    def stop(self):
        self.mouse_listener.stop()

    def mouse_stop_callback(self, mouse_position: Tuple[int, int], *args, **kwargs):
        if DebugFlags.print_detection_profiling:
            import cProfile
            cProfile.runctx("self.translate()", globals(), locals())
        else:
            self.translate()

    def translate(self):

        image, dpi = self.image_shooter.get_screenshot(position=self.mouse.get_current_position())
        text = self.text_detector.get_text(image_or_path=image, dpi=dpi)
        translated_text = self.translator.translate(text)

        if DebugFlags.print_text_translation:
            print(f"====================================")
            print(f"{self.__class__}: detected text under mouse:\n")
            print(f"{text}")
            print(f"===============")

            print(f"{self.__class__}: translated text under mouse:\n")
            print(f"{translated_text}")
            print(f"===============")

        if translated_text != "":
            self.osd.set_text(translated_text)
            self.osd.show()


def main():
    print(f"*** [{threading.current_thread().ident}] main() called...")

    # DebugFlags.print_detection_profiling = True

    app = PentacApp()  # theme_name=themes.DraculaTheme.name)
    app.execute()


if __name__ == '__main__':
    main()
