from typing import Union

import pytesseract
import cv2
import pathlib
import argparse
import tempfile
import sys
import numpy as np


class TextDetection:

    def __init__(self, tesseract_executable_path: pathlib.Path):

        self.tesseract_executable_path = tesseract_executable_path

        self.result_resolution_dpi = 500
        self.preprocessed_file_name = "preprocessed.jpg"

        self.show_preprocessed_image = False
        self.save_preprocessed_image = False

    def _configure(self):
        pytesseract.pytesseract.tesseract_cmd = str(self.tesseract_executable_path)

    def _resize(self, image: np.ndarray, dpi: float) -> np.ndarray:
        height, width = image.shape[:2]

        preprocess_resize_factor = self.result_resolution_dpi / dpi
        print(f"*** resize original image [width={width},height={height}]@{dpi} with factor {preprocess_resize_factor} "
              f"to reach result dpi of {self.result_resolution_dpi}")
        resized_image = cv2.resize(image,
                                   (int(preprocess_resize_factor * width), int(preprocess_resize_factor * height)),
                                   interpolation=cv2.INTER_CUBIC)
        return resized_image

    @staticmethod
    def _convert_to_grayscale(image: np.ndarray) -> np.ndarray:
        gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        return gray_image

    @staticmethod
    def _threshold(image: np.ndarray) -> np.ndarray:
        _, threshold_image = cv2.threshold(image, 0, 255, cv2.THRESH_OTSU | cv2.THRESH_BINARY)

        # gray = cv2.adaptiveThreshold(gray, 200, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 11, 2)

        # gray = cv2.medianBlur(gray, 1)
        # gray = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]

        return threshold_image

    @staticmethod
    def _write_preprocessed_file(image: np.ndarray):
        preprocessed_file = tempfile.mktemp(prefix="preprocessed_", suffix=".jpg")
        cv2.imwrite(str(preprocessed_file), image)
        print(f"*** Saved preprocess image as \"{str(preprocessed_file)}\"")

    def _show_preprocessed_image(self, image: np.ndarray):
        cv2.imshow(self.preprocessed_file_name, image)
        cv2.waitKey(0)

    def get_preprocessed_image(self, image: np.ndarray, dpi: float) -> np.ndarray:

        image = self._resize(image, dpi)
        image = self._convert_to_grayscale(image)
        image = self._threshold(image)

        return image

    def get_text(self, image_or_path: Union[np.ndarray, pathlib.Path], dpi: float = None):

        self._configure()

        if issubclass(type(image_or_path), pathlib.Path):
            image = cv2.imread(str(image_or_path))
        else:
            image = image_or_path

        image = self.get_preprocessed_image(image, dpi)

        if self.save_preprocessed_image:
            self._write_preprocessed_file(image)

        if self.show_preprocessed_image:
            self._show_preprocessed_image(image)

        # return pytesseract.image_to_string(image, config="--psm 6 -c classify_enable_learning=false")
        return pytesseract.image_to_string(image, config="--psm 6")


def main():
    parsed_args = parse_arguments(sys.argv[1:])

    parsed_args.input_file: pathlib.Path

    detector = TextDetection()
    text = detector.get_text(parsed_args.input_file)

    print(f"The image contains the text \"{text}\".")


def parse_arguments(args):
    parser = argparse.ArgumentParser(description='This module uses tesseract to transform images to text.')
    parser.add_argument("-i", "--input_file", type=pathlib.Path, required=True,
                        help='Path of image file to be scanned for text.')

    return parser.parse_args(args)


if __name__ == '__main__':
    main()
