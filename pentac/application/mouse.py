from pynput import mouse
import threading
from typing import Tuple, Callable
import tkinter as tk


class MouseListener:

    def __init__(self, time_after_mouse_stop_s: float, callback: Callable[[Tuple[int, int]], None]):

        self.mouse_listener: mouse.Listener = None

        self.mouse_move_event_count = 0

        self.time_after_mouse_stop_s = time_after_mouse_stop_s
        self.callback: Callable[[Tuple[int, int]], None] = callback

        self.timer: threading.Timer = None

        self.last_mouse_position: Tuple[int, int] = (0, 0)

    def start_listening(self, blocking=False, auto_stop_after_click=False):
        if auto_stop_after_click:
            self.mouse_listener = mouse.Listener(on_move=self._on_move,
                                                 on_click=self._on_click)
        else:
            self.mouse_listener = mouse.Listener(on_move=self._on_move)

        self.mouse_listener.start()

        self._restart_timer()

        if blocking:
            self.mouse_listener.join()

    def stop(self):
        self.mouse_listener.stop()
        self.timer.cancel()

    def get_mouse_movement_count(self):
        return self.mouse_move_event_count

    def _restart_timer(self):
        if self.timer is not None:
            self.timer.cancel()

        self.timer = threading.Timer(self.time_after_mouse_stop_s, self._timeout_occurred)
        self.timer.start()

    def _timeout_occurred(self):
        self.callback(self.last_mouse_position)

    def _on_move(self, x, y):
        self.last_mouse_position = (x, y)
        self._restart_timer()
        self.mouse_move_event_count += 1

    def _on_click(self, x, y, button, pressed):
        if not pressed:
            self.stop()


class MousePosition:

    @staticmethod
    def get_current_position() -> Tuple[int, int]:
        root = tk.Tk()

        x = root.winfo_pointerx()
        y = root.winfo_pointery()

        abs_coord_x = root.winfo_pointerx() - root.winfo_rootx()
        abs_coord_y = root.winfo_pointery() - root.winfo_rooty()

        return abs_coord_x, abs_coord_y


class TestCallbackHandler:

    def __init__(self):
        self.name = "TestCallbackHandler"

    def test_callback(self, mouse_position: Tuple[int, int], *args, **kwargs):
        print(f"{self.name}: callback received with mouse_position={mouse_position}, args={args} and kwargs={kwargs}...")


def main():
    test_handler = TestCallbackHandler()

    listener = MouseListener(time_after_mouse_stop_s=2.0,
                             callback=test_handler.test_callback)
    listener.start_listening(blocking=True,
                             auto_stop_after_click=True)


if __name__ == "__main__":
    main()
