from typing import List, Dict
import re
import os

from pentac.plugin import PluginManager, PluginInterface
from pentac.configuration import Plugins


class TranslationHandler:

    markdown_chapter_separator = "\n\n"

    def __init__(self, plugins_configuration: Plugins):
        self.plugins = PluginManager(plugins_configuration)
        self.plugins.load_plugins()
        self.plugins.print_identification()
        self.plugins.setup()

        self.plugin_configurations = self.plugins.get_configurations()

        self.patterns: Dict[PluginInterface, List[re.Pattern]] = {}

        self.translation_match_separator = TranslationHandler.markdown_chapter_separator
        self.translation_plugin_separator = TranslationHandler.markdown_chapter_separator

        self._configure_matching()

    def _configure_matching(self):
        for plugin, plugin_config in self.plugin_configurations.items():
            compiled_patterns = []
            for pattern_string in plugin_config.match_patterns:
                try:
                    compiled_patterns.append(re.compile(pattern_string))
                except re.error as e:
                    raise SyntaxError(f"{plugin.get_identification()} has invalid match_pattern \"{pattern_string}\"") \
                        from e
            self.patterns[plugin] = compiled_patterns

    def translate(self, text):
        translated_text = ""

        for plugin, patterns in self.patterns.items():
            for index, pattern in enumerate(patterns):

                all_matches = pattern.findall(text)
                if len(all_matches) > 0:

                    matches_to_translate = [all_matches[-1]]
                    if not self.plugin_configurations[plugin].match_only_nearest_to_mouse:
                        matches_to_translate = all_matches

                    for match in matches_to_translate:
                        translated_text += plugin.get_translation(match, pattern.pattern, index) + self.translation_match_separator

                    translated_text += self.translation_plugin_separator

        cleaned_text_lines = [line for line in translated_text.splitlines()]
        if len(cleaned_text_lines) > 0:
            cleaned_text_lines.pop()
        cleaned_text = os.linesep.join(cleaned_text_lines)

        return cleaned_text

        # print(f"Plugin[{plugin.get_identification()} pattern {pattern}")

