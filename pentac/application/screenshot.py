import traceback

import numpy as np
from typing import Tuple, Dict
import mss as mss

from PySide6.QtWidgets import QApplication
from PySide6.QtCore import QPoint

from colorama import init
from mss.screenshot import ScreenShot
from termcolor import colored
init()


class ScreenDpiInfo:

    dots_per_inch_fallback = 94

    def __init__(self, q_application: QApplication = None):
        self.app = q_application
        if self.app is None:
            self.app = QApplication()

    def get_monitor_dpi_at(self, position: Tuple[int, int]) -> float:
        x, y = position
        screen = self.app.screenAt(QPoint(x, y))

        try:
            dots_per_inch = screen.physicalDotsPerInch()
        except Exception:
            dots_per_inch = ScreenDpiInfo.dots_per_inch_fallback
            print(colored(f"screen.physicalDotsPerInch() raised exception -> assume {dots_per_inch}dpi. "
                          f"See stack trace:", "red", attrs=["bold", "underline"]))
            traceback.print_exc()

        return dots_per_inch


class ScreenShot:

    def __init__(self, height, width, q_application: QApplication = None):
        self.height = height
        self.width = width

        self.screen_shooter = mss.mss()
        self.dpi_info = ScreenDpiInfo(q_application)

    def get_screenshot(self, position: Tuple[int, int]) -> (np.ndarray, float):
        x, y = position

        screenshot_region = {"top": y - self.height, "left": x - self.width, "width": self.width, "height": self.height}
        print(f"*** screenshot_region={screenshot_region}")

        screenshot = self._grab(screenshot_region)
        screenshot_image = np.array(screenshot)

        opencv_image = np.array(screenshot_image)
        return opencv_image, self.dpi_info.get_monitor_dpi_at(position)

    def _grab(self, screenshot_region: Dict[str, int]) -> ScreenShot:
        """
        Try to grab() and if exception is coming initialize a new mss object.
        This is happening on some resolution changes etc.
        """

        try:
            screenshot = self.screen_shooter.grab(monitor=screenshot_region)
        except Exception:
            print(colored(f"mss.grab() raised exception -> try to create new mss object."
                          f"See stack trace:", "red", attrs=["bold", "underline"]))
            traceback.print_exc()
            self.screen_shooter.close()
            self.screen_shooter = mss.mss()
            print(colored(f"Try to grab with new object...", "yellow", attrs=["bold", "underline"]))
            screenshot = self.screen_shooter.grab(monitor=screenshot_region)
            print(colored(f",,, worked", "green", attrs=["bold", "underline"]))

        return screenshot


def main():
    pass


if __name__ == '__main__':
    main()
