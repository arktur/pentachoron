import pathlib
import sys

from pentac.configuration import ConfigurationLoader

from PySide6.QtGui import QIcon, Qt, QPalette
from PySide6.QtWidgets import QSystemTrayIcon, QMenu, QApplication, QWidget


class TrayMenu:

    def __init__(self, q_application: QApplication):

        self.q_application = q_application

        self.widget: QWidget = QWidget()

        self.menu: QMenu = QMenu(self.widget)
        self.menu_configuration: QMenu = None

        self.exit_action = None

        self.icon_path: pathlib.Path = None
        self.trayIcon: TrayMenu = None

    def add_configuration_info(self, config_loader: ConfigurationLoader):
        self.menu_configuration = self.menu.addMenu("Configuration")

        self.menu_configuration.addAction("Used config file:")
        self.menu_configuration.addAction(f"    {str(config_loader.get_used_config_file())}")
        self.menu_configuration.addSeparator()

        self.menu_configuration.addAction("Configuration used (loaded / overwritten / added):")
        config_string = str(config_loader.configuration)
        for line in config_string.splitlines():
            if line != "":
                self.menu_configuration.addAction(f"    {line}")

    def show(self, tray_color: str):
        self.exit_action = self.menu.addAction("Exit")
        self.exit_action.triggered.connect(self._exit)

        self.icon_path = pathlib.Path(__file__).parent.joinpath("icons").joinpath(f"tray_{tray_color}.png")
        self.trayIcon = QSystemTrayIcon(QIcon(str(self.icon_path)), self.widget)

        self.trayIcon.setContextMenu(self.menu)

        self._activate_theme()

        self.trayIcon.show()

    def _activate_theme(self):
        background_color = self.q_application.palette().color(QPalette.Window).name()
        self.menu.setStyleSheet(f"background-color: {background_color};")  # color: {line};");

    def _exit(self):
        self.q_application.exit(0)


def main():
    app = QApplication(sys.argv)

    tray = TrayMenu()
    tray.show(tray_color="green")

    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
