import os
import sys
import threading
from typing import Callable

from PySide6.QtCore import QTimer, Qt
from PySide6.QtWidgets import QApplication


class Application:

    application_name = "Pentachoron"

    def __init__(self, internal_timeout_ms: int = 100):
        os.environ["QT_AUTO_SCREEN_SCALE_FACTOR"] = "1"
        self.q_application = QApplication(sys.argv)
        self.q_application.setAttribute(Qt.AA_EnableHighDpiScaling)

        self.q_application.setApplicationName(Application.application_name)
        self.q_application.setApplicationDisplayName(Application.application_name)

        self.timer = QTimer()
        self.timer.timeout.connect(lambda: None)
        self.timer.start(internal_timeout_ms)

        self.parallel_worker: threading.Thread = None

    @staticmethod
    def exception_hook(exception_type, value, traceback):
        print("+--------------------------------------------------------+")
        print("|    Exception hook called... Here are the details:      |")
        print(exception_type, value, traceback)
        print("+--------------------------------------------------------+")
        # Call the normal Exception hook after
        sys.__excepthook__(exception_type, value, traceback)
        sys.exit(value)

    def execute(self, parallel_worker: Callable = None, *args, **kwargs):
        sys.excepthook = Application.exception_hook

        if parallel_worker is not None:
            self.parallel_worker = threading.Thread(target=parallel_worker,
                                                    args=args, kwargs=kwargs,
                                                    daemon=True, name=self.__class__)
            self.parallel_worker.start()

        sys.exit(self.q_application.exec_())
