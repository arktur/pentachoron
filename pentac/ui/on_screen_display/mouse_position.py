from typing import Tuple
from PySide6 import QtGui


class MousePosition:

    @staticmethod
    def get_current_position() -> Tuple[int, int]:

        position = QtGui.QCursor.pos()
        return position.x(), position.y()
