import threading
import ctypes


class StoppableThread(threading.Thread):
    """Thread class with a stop() method. The method forces the thread to stop without a need to poll."""

    def __init__(self, *args, **kwargs):
        super(StoppableThread, self).__init__(*args, **kwargs)

    def _get_thread_id(self):
        # returns id of the respective thread
        for id, thread in threading._active.items():
            if thread is self:
                return id

    def stop(self):
        thread_id = self._get_thread_id()

        result = ctypes.pythonapi.PyThreadState_SetAsyncExc(thread_id,
                                                            ctypes.py_object(SystemExit))
        if result == 0:
            raise ValueError("Invalid thread ID")
        elif result > 1:
            ctypes.pythonapi.PyThreadState_SetAsyncExc(thread_id, 0)
            print('Exception raise failure')
