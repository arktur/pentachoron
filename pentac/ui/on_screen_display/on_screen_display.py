import time
from PySide6.QtCore import Qt, QRect, QPoint, QObject, Signal
from PySide6.QtGui import QPalette
from PySide6.QtWidgets import QApplication, QFrame, QWidget, QGridLayout, QLabel, QLayout

from pentac.ui.on_screen_display import themes
from pentac.ui.on_screen_display.mouse_position import MousePosition
from pentac.ui.application import Application

from pynput import mouse

import threading


class Signals(QObject):
    set_text = Signal(str)
    set_position = Signal(int, int)
    show = Signal()
    hide = Signal()


class OnScreenDisplay:

    def __init__(self, q_application: QApplication,
                 theme_name: str = themes.DraculaTheme.name, transparency_percent: float = 20,
                 close_on_mouse_leave: bool = True, follow_mouse_position: bool = True, show_on_creation: bool = False):

        self.signals: Signals = None

        self.theme_name = theme_name
        self.transparency_percent = transparency_percent
        self.close_on_mouse_leave = close_on_mouse_leave
        self.follow_mouse_position = follow_mouse_position

        self.app: QApplication = q_application

        self.widget: QWidget = None
        self.text: QLabel = None
        self.layout: QGridLayout = None

        self.ui_initialized: bool = False

        self.show_on_creation = show_on_creation
        self.mouse_listener: mouse.Listener = mouse.Listener(on_move=self._on_mouse_move)
        self.mouse_listener.start()

    def _setup_signals(self):
        self.signals = Signals()
        self.signals.set_text.connect(self._set_text)
        self.signals.set_position.connect(self._set_position)
        self.signals.show.connect(self._show)
        self.signals.hide.connect(self._hide)

    def start(self):
        self._build_ui()
        self._setup_signals()

        self.show()
        if not self.show_on_creation:
            self.hide()

        self.ui_initialized = True

    def _build_ui(self):
        print(f"*** [{threading.current_thread().ident}] _build_ui() is building the widget...")

        self.widget = QWidget()

        # Qt.Tool hides task bar icon.
        flags = Qt.WindowFlags(Qt.FramelessWindowHint | Qt.WindowStaysOnTopHint | Qt.Tool)
        self.widget.setWindowFlags(flags)
        self.widget.setWindowOpacity(1.0 - (self.transparency_percent / 100))

        self.line = QFrame()
        self.line.setGeometry(QRect(120, 100, 118, 3))
        self.line.setFrameShape(QFrame.HLine)
        self.line.setFrameShadow(QFrame.Sunken)

        self.text = QLabel(text="   ")
        self.text.setTextInteractionFlags(Qt.TextSelectableByMouse |
                                          Qt.TextSelectableByKeyboard |
                                          Qt.TextEditorInteraction |
                                          Qt.TextBrowserInteraction)
        self.text.setOpenExternalLinks(True)
        self.text.setTextFormat(Qt.TextFormat.MarkdownText)

        self.layout = QGridLayout(self.widget)
        self.layout.setSizeConstraint(QLayout.SetFixedSize)

        self.layout.addWidget(self.line, 0, 0)
        self.layout.addWidget(self.text, 1, 0)

        self._activate_theme(self.theme_name)

    def _activate_theme(self, theme_name: str = ""):
        if themes.DraculaTheme.name == theme_name:
            self._activate_theme_colors(line=themes.DraculaTheme.colored_application,
                                        background=themes.DraculaTheme.dark_purple,
                                        text=themes.DraculaTheme.cyan,
                                        disabled_text=themes.DraculaTheme.grey,
                                        links_unvisited=themes.DraculaTheme.pink,
                                        links_visited=themes.DraculaTheme.pink)
        elif themes.CleanDarkTheme.name == theme_name:
            self._activate_theme_colors(line=themes.CleanDarkTheme.colored_application,
                                        background=themes.CleanDarkTheme.background_main,
                                        text=themes.CleanDarkTheme.text_active_control,
                                        disabled_text=themes.CleanDarkTheme.text_inactive_control,
                                        links_unvisited=themes.CleanDarkTheme.colored_application,
                                        links_visited=themes.CleanDarkTheme.colored_application)
        else:
            self._activate_theme_colors()

    def _activate_theme_colors(self, line: str = f"#ffff81", background: str = Qt.black, text: str = Qt.red,
                               disabled_text: str = Qt.darkGray,
                               links_unvisited: str = Qt.green, links_visited: str = Qt.green):

        # Some hints on color palette: https://gist.github.com/lschmierer/443b8e21ad93e2a2d7eb

        # Maybe switch to stylesheets later...
        #
        # see examples: http://epic-alfa.kavli.tudelft.nl/share/doc/qt4/html/stylesheet-examples.html
        # see reference: http://epic-alfa.kavli.tudelft.nl/share/doc/qt4/html/stylesheet-reference.html
        #
        # style_sheet = " QFrame, QLabel, QToolTip {" \
        #               "       border: 2px solid green;" \
        #               "       border-radius: 4px;" \
        #               "       padding: 2px;" \
        #               "   }"
        # self.app.setStyleSheet(style_sheet)

        self.line.setStyleSheet(f"background-color: {line};")  # color: {line};");
        qp = QPalette()
        qp.setColor(QPalette.WindowText, text)
        qp.setColor(QPalette.Window, background)
        qp.setColor(QPalette.Button, line)

        qp.setColor(QPalette.Base, background)
        qp.setColor(QPalette.AlternateBase, background)

        qp.setColor(QPalette.Link, links_unvisited)
        qp.setColor(QPalette.LinkVisited, links_visited)

        # For context menu
        qp.setColor(QPalette.Text, text)
        qp.setColor(QPalette.ButtonText, text)
        qp.setColor(QPalette.Disabled, QPalette.Text, disabled_text)
        qp.setColor(QPalette.Disabled, QPalette.ButtonText, disabled_text)
        self.app.setPalette(qp)

        # dark_palette = QPalette()
        # dark_palette.setColor(QPalette.Window, QColor(53, 53, 53))
        # dark_palette.setColor(QPalette.WindowText, Qt.white)
        # dark_palette.setColor(QPalette.Base, QColor(25, 25, 25))
        # dark_palette.setColor(QPalette.AlternateBase, QColor(53, 53, 53))
        # dark_palette.setColor(QPalette.ToolTipBase, Qt.white)
        # dark_palette.setColor(QPalette.ToolTipText, Qt.white)
        # dark_palette.setColor(QPalette.Text, Qt.white)
        # dark_palette.setColor(QPalette.Button, QColor(53, 53, 53))
        # dark_palette.setColor(QPalette.ButtonText, Qt.white)
        # dark_palette.setColor(QPalette.BrightText, Qt.red)
        # dark_palette.setColor(QPalette.Link, QColor(42, 130, 218))
        # dark_palette.setColor(QPalette.Highlight, QColor(42, 130, 218))
        # dark_palette.setColor(QPalette.HighlightedText, Qt.black)
        # dark_palette.setColor(QPalette.Disabled, QPalette.Text, disabled_text)
        # dark_palette.setColor(QPalette.Disabled, QPalette.ButtonText, disabled_text)
        # self.app.setPalette(dark_palette)

        self.app.setStyleSheet("QToolTip { color: #ffffff; background-color: #2a82da; border: 1px solid white; }")

    def _on_mouse_move(self, x, y):
        if not self.ui_initialized:
            return
        if self.widget.isHidden():
            return

        x, y = MousePosition.get_current_position()
        if self.close_on_mouse_leave and not self._position_over_window(x, y):
            self.hide()

    def _move_to_mouse_cursor(self):
        if self.follow_mouse_position:
            x, y = MousePosition.get_current_position()
            size = self.widget.size()
            width, height = size.width(), size.height()
            self.set_position(x, y - height)

    def _position_over_window(self, x, y) -> bool:
        mouse_x, mouse_y = x, y  # MousePosition.get_current_position()

        widget_pos = self.widget.pos()
        widget_size = self.widget.size()
        current_x, current_y = widget_pos.x(), widget_pos.y()
        current_width, current_height = widget_size.width(), widget_size.height()

        left = current_x
        right = current_x + current_width
        top = current_y
        bottom = current_y + current_height

        # print(f"window_x={current_x}, window_y={current_y}, "
        #       f"mouse_x={mouse_x}, mouse_y={mouse_y}, "
        #       f"left={left}, right={right}, top={top}, bottom={bottom}")

        return left <= mouse_x <= right and top <= mouse_y <= bottom

    def show(self):
        print(f"*** [{threading.current_thread().ident}] before .show() emitting the stuff...")
        self.signals.show.emit()

    def _show(self):
        print(f"*** [{threading.current_thread().ident}] after ._show() got the stuff...")
        if self.widget is None:
            self.show_on_creation = True
            return
        if self.follow_mouse_position:
            self._move_to_mouse_cursor()
        self.widget.show()

    def hide(self):
        print(f"*** [{threading.current_thread().ident}] before .hide() emitting the stuff...")
        self.signals.hide.emit()

    def _hide(self):
        print(f"*** [{threading.current_thread().ident}] after ._hide() got the stuff...")
        if self.widget is None:
            self.show_on_creation = False
            return
        self.widget.hide()

    def set_text(self, text):
        print(f"*** [{threading.current_thread().ident}] before .set_text() emitting the stuff...")
        self.signals.set_text.emit(text)

    def _set_text(self, text):
        print(f"*** [{threading.current_thread().ident}] connected ._set_text() got the stuff [text={text}]...")
        if self.widget is None:
            return
        self.text.setText(text)

    def set_position(self, x, y):
        print(f"*** [{threading.current_thread().ident}] before .set_position() emitting the stuff...")
        self.signals.set_position.emit(x, y)

    def _set_position(self, x, y):
        print(f"*** [{threading.current_thread().ident}] connected ._set_position() got the stuff [x={x},y={y}]...")
        self.widget.move(QPoint(x, y))


def example_handling(app: Application, osd: OnScreenDisplay):
    osd.show()
    time.sleep(2)
    osd.hide()
    time.sleep(2)
    osd.show()
    osd.set_text("02 - neuer schöner text")
    time.sleep(5)
    osd.hide()
    time.sleep(2)
    osd.set_text("03 - VIEL Längerer Text - neuer schöner text")
    osd.show()
    time.sleep(2)
    osd.set_text("04 - wieder kürzer")
    time.sleep(2)

    app.q_application.exit(0)


def example_link(app: Application, osd: OnScreenDisplay):
    ticket_number = "LMCFW-4598"
    link = f"<a href=\"http://jira.msol.schneider-electric.com/browse/{ticket_number}\">{ticket_number}</a>"
    text = f"- other result string\n"
    text += f"- list 2\n"
    text += f"\n"
    text += link
    osd.set_text(text)
    osd.show()


def main():
    app = Application()

    osd = OnScreenDisplay(q_application=app.q_application,
                          theme_name=themes.DraculaTheme.name, transparency_percent=20,
                          close_on_mouse_leave=False, follow_mouse_position=True, show_on_creation=False)
    osd.start()

    app.execute(parallel_worker=example_link, app=app, osd=osd)


if __name__ == '__main__':
    main()
