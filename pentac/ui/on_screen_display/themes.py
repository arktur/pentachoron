class Theme:

    colored_application = 'white'


class DraculaTheme(Theme):

    name = 'dracula'

    darker_purple = '#151320'
    dark_purple = '#22212c'
    middle_purple = '#2b2640'
    light_purple = '#7970aa'
    green = '#89fd7f'
    pink = '#ff81c1'
    orange = '#fac67e'
    yellow = '#ffff81'
    white = '#f3f3ee'
    cyan = '#80ffea'
    grey = '#a0a0a0'

    colored_application = green


class CleanDarkTheme(Theme):

    name = 'clean_dark'

    background_main = '#282828'
    background_sidebar = '#1e1e1e'
    active_marker = '#409eff'
    active_marker_darker = '#277bff'
    active_control = '#ffffff'
    text_inactive_control = '#7d7d7d' #'#9ca5a5'
    text_active_control = '#c7d2d2'
    icons = '#6a9fb5'
    scroll_bar = '#3e3e3e'
    scroll_button = background_main

    colored_application = active_marker_darker
