import pathlib
from abc import ABC as ABSTRACT_BASE_CLASS
from abc import abstractmethod
from typing import List


class PluginIdentification:
    def __init__(self, name: str, version: str):
        self.name = name
        self.version = version


class PluginConfiguration:
    def __init__(self, match_patterns: List[str], match_only_nearest_to_mouse: bool):
        self.match_patterns = match_patterns
        self.match_only_nearest_to_mouse = match_only_nearest_to_mouse


class PluginInterface(ABSTRACT_BASE_CLASS):

    def get_identification_string(self) -> str:

        identification = self.get_identification()

        class_path = f"{self.__module__}.{type(self).__name__}"
        instance_address = f"{hex(id(self))}]"

        return f"pentac.plugin[name={identification.name},version={identification.version}]" \
               f".instance[of={class_path},@{instance_address}]"

    @abstractmethod
    def get_identification(self) -> PluginIdentification:
        raise NotImplementedError

    @abstractmethod
    def set_plugin_configuration(self, configuration: str = "", config_file: str = None):
        raise NotImplementedError

    @abstractmethod
    def get_configuration(self) -> PluginConfiguration:
        raise NotImplementedError

    @abstractmethod
    def get_translation(self, text, match_pattern, match_pattern_index) -> str:
        raise NotImplementedError
