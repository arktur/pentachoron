from typing import List

from pentac.configuration import Plugin as PluginConfig
from pentac.plugin import PluginManager


def main():
    plugins: List[PluginConfig] = [PluginConfig(module="pentac.plugins.example.example_plugin"),
                                   PluginConfig(module="pentac.plugins.example.example_plugin",
                                                from_file=r"D:\git\pentachoron\pentac\plugins\example\example_plugin.py")]
    plugin_manager = PluginManager(plugin_configuration=plugins)
    plugin_manager.load_plugins()
    plugin_manager.setup()
    plugin_manager.print_identification()


if __name__ == '__main__':
    main()
