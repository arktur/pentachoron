import importlib
import pathlib
from typing import List, Dict

from pentac.configuration import Plugin as PluginConfig
from .plugin_interface import PluginInterface, PluginConfiguration

from colorama import init
from termcolor import colored
init()


class Plugin:

    def __init__(self, module: str, instance: PluginInterface, config: PluginConfig):
        self.module: str = module  #
        self.instance: PluginInterface = instance
        self.configuration: PluginConfig = config


class PluginManager(object):
    registered_plugins: List[PluginInterface] = []

    def __init__(self, plugin_configuration: List[PluginConfig]):
        self.plugin_configurations: List[PluginConfig] = plugin_configuration
        self.loaded_plugins: List[Plugin] = []

    def load_plugins(self):

        for plugin_config in self.plugin_configurations:

            previously_registered_plugins = len(PluginManager.registered_plugins)

            self._import_plugin_module(plugin_config)

            new_registered_plugins = self._get_registered_plugin_instances(previously_registered_plugins)

            for plugin_instance in new_registered_plugins:
                loaded_plugin = Plugin(module=plugin_config.module, instance=plugin_instance, config=plugin_config)
                self.loaded_plugins.append(loaded_plugin)

    @staticmethod
    def _get_registered_plugin_instances(registered_count):
        new_registered_plugins_count = len(PluginManager.registered_plugins) - registered_count
        if 0 == new_registered_plugins_count:
            raise IndexError("No plugins registered in configured module.")
        new_registered_plugins = PluginManager.registered_plugins[registered_count:
                                                                  registered_count + new_registered_plugins_count]
        return new_registered_plugins

    @staticmethod
    def _import_plugin_module(plugin_config):
        if plugin_config.from_file is None:
            importlib.import_module(plugin_config.module)
        else:
            path = pathlib.Path(plugin_config.from_file)
            spec = importlib.util.spec_from_file_location(plugin_config.module, str(path))
            import_module = importlib.util.module_from_spec(spec)
            spec.loader.exec_module(import_module)

    def print_identification(self):
        for plugin in self.loaded_plugins:
            print(colored(plugin.instance.get_identification_string(), "green"))

    def setup(self):
        for plugin in self.loaded_plugins:
            plugin.instance.set_plugin_configuration(configuration=plugin.configuration.configuration,
                                                     config_file=plugin.configuration.configuration_file)

    def get_configurations(self) -> Dict[PluginInterface, PluginConfiguration]:
        configuration_list = {}
        for plugin in self.registered_plugins:
            configuration_list[plugin] = plugin.get_configuration()
        return configuration_list

    @classmethod
    def plugin(cls, plugin: PluginInterface.__class__):
        plugin_instance = plugin()
        cls.registered_plugins.append(plugin_instance)


def main():
    pass


if __name__ == '__main__':
    main()
