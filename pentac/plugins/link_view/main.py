import pathlib
from pentac.application import PentacApp
from pentac.application.controller import DebugFlags
from pentac.configuration import Plugin
import yaml


def main():
    DebugFlags.print_text_translation = True

    # plugins = [Plugin(module="pentac.plugins.link_view.plugin",
    #                   configuration=yaml.safe_load("""
    #                 - match_patterns:                             # List of all patterns that should be transformed to links
    #                     - "[#]{1}[0-9]{1,4}"
    #                   links:                                      # List of all links that can be displayed
    #                     - for_match_pattern: "[#]{1}[0-9]{1,4}"   # Optional: Link should only be displayed for specific match pattern
    #                       text_replacements:                      # Optional: replace stuff in source text before going on with link creation.
    #                         - replace: "#"
    #                           with: ""
    #                       link: https://gitlab.com/arktur/pentachoron/-/issues/[[text]]   # Link to create. [[text]] can be used to insert the text including all configured replacements.
    #                   """))]
    app = PentacApp(
                    #plugins=plugins,
                    config_file=pathlib.Path(__file__).parent.joinpath("tmp.yaml")
    )

    app.execute()


if __name__ == '__main__':
    main()
