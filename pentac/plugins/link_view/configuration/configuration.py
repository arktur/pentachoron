import pathlib
import yaml
from typing import List, Union, Hashable, Any, Dict


class ConfigurationObject:

    depth: int = -1
    indentation: str = "    "

    def __repr__(self):

        ConfigurationObject.depth += 1
        indent = ConfigurationObject.depth * ConfigurationObject.indentation

        representation = "\n"
        for item, value in self.__dict__.items():
            representation += f"{indent}{item}: {value}\n"

        ConfigurationObject.depth -= 1
        return representation


class LinkViewConfig(ConfigurationObject):
    def __init__(self, link_config):
        self.match_patterns: List[str] = None
        self.links: List[LinkConfig] = None

        for option in link_config:
            if "links" == option:
                value = LinksConfig(link_config[option])
            else:
                value = link_config[option]
            self.__setattr__(option, value)


class LinkViewsConfig(list, ConfigurationObject):

    def __init__(self):
        super().__init__()

    @classmethod
    def from_file(cls, file: pathlib.Path) -> List[LinkViewConfig]:
        config = cls()
        parsed_config = config.load_config(file)
        config._create_attributes(parsed_config)
        return config

    @staticmethod
    def load_config(config_file: pathlib.Path):
        with open(config_file, 'r') as file:
            return yaml.safe_load(file)

    @classmethod
    def from_parsed_yaml(cls, configuration: Union[Dict[Hashable, Any], List[Any], None]) -> List[LinkViewConfig]:
        """
        Keyword arguments:
        configuration -- a parsed yaml configuration returned e.g. by yaml.safe_load(text).
        """
        config = cls()
        config._create_attributes(configuration)
        return config

    @classmethod
    def from_text(cls, configuration: str) -> List[LinkViewConfig]:
        """
        Keyword arguments:
        configuration -- a string that will be parsed with yaml.safe_load(configuration).
        """
        config = cls()
        parsed_config = yaml.safe_load(configuration)
        config._create_attributes(parsed_config)
        return config

    def _create_attributes(self, configuration_list):
        for link_config in configuration_list:
            self.append(LinkViewConfig(link_config=link_config))


class LinksConfig(list, ConfigurationObject):
    def __init__(self, links_config):
        super().__init__()
        for link_config in links_config:
            self.append(LinkConfig(link_config))


class LinkConfig(ConfigurationObject):
    def __init__(self, link_config):
        self.for_match_pattern: str = None
        self.text_replacements: List[TextReplacementConfig] = None
        self.link: str = None

        for option in link_config:
            if "text_replacements" == option:
                replacements_list = []
                text_replacements = link_config[option]
                for text_replacement in text_replacements:
                    replacements_list.append(TextReplacementConfig(text_replacement))
                value = replacements_list
            else:
                value = link_config[option]
            self.__setattr__(option, value)


class TextReplacementConfig(ConfigurationObject):
    def __init__(self, text_replacement_config):
        self.replace: str = None
        self.with_: str = None

        for option in text_replacement_config:
            value = text_replacement_config[option]
            if "with" == option:
                self.__setattr__("with_", value)
            else:
                self.__setattr__(option, value)


def main():
    #default_config_file = pathlib.Path(__file__).parent.joinpath("config_template.yaml")
    #config = LinkViewsConfig.from_file(file=default_config_file)

    configuration = """
    - match_patterns:                             # List of all patterns that should be transformed to links
        - "#[0-9]{1-4}"
      links:                                      # List of all links that can be displayed
        - for_match_pattern: "#[0-9]{1-4]"        # Optional: Link should only be displayed for specific match pattern
          text_replacements:                      # Optional: replace stuff in source text before going on with link creation.
            - replace: "#"
              with: ""
          link: https://gitlab.com/arktur/pentachoron/-/issues/[[text]]   # Link to create. [[text]] can be used to insert the text including all configured replacements.    
    """

    config = LinkViewsConfig.from_parsed_yaml(configuration=configuration)
    print(config)


if __name__ == '__main__':
    main()
