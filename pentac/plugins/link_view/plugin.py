import pathlib
from typing import List

from pentac.plugin import PluginManager, PluginInterface, PluginIdentification, PluginConfiguration
from .configuration import LinkViewsConfig, LinkViewConfig


@PluginManager.plugin
class LinkView(PluginInterface):

    def __init__(self):
        self.identification = PluginIdentification(name="LinkView", version="0.0.1")
        self.configuration: PluginConfiguration = None
        self.plugin_config: List[LinkViewConfig] = None

    def get_identification(self) -> PluginIdentification:
        return self.identification

    def set_plugin_configuration(self, configuration: str = "", config_file: pathlib.Path = None):

        if config_file is not None:
            raise NotImplementedError("LinkView plugin does not support config_files yet. Please insert your config"
                                      "into the configuration parameter inside pentac config.yaml directly.")

        self.plugin_config = LinkViewsConfig.from_parsed_yaml(configuration=configuration)

        match_patterns = []
        for link_view in self.plugin_config:
            match_patterns.extend(link_view.match_patterns)

        self.configuration = PluginConfiguration(match_patterns=match_patterns,
                                                 match_only_nearest_to_mouse=True)

    def get_configuration(self) -> PluginConfiguration:
        return self.configuration

    def get_translation(self, text, match_pattern, match_pattern_index) -> str:

        all_links = []

        for link_view in self.plugin_config:
            for link in link_view.links:
                if link.for_match_pattern is None or link.for_match_pattern == match_pattern:
                    matched_text = text
                    if link.text_replacements is not None:
                        for replacement in link.text_replacements:
                            matched_text = matched_text.replace(replacement.replace, replacement.with_)
                    link_text = link.link
                    link_text = link_text.replace("[[text]]", matched_text)
                    all_links.append(f"<a href=\"{link_text}\">{text}</a>")

        return_value = ""
        for index, link in enumerate(all_links):
            if index > 0:
                return_value += "\n"
            return_value += f"{link}"

        return return_value
