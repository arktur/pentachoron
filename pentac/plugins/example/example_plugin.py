import pathlib

from pentac.plugin import PluginManager, PluginInterface, PluginIdentification, PluginConfiguration


@PluginManager.plugin
class ExamplePlugin(PluginInterface):

    def __init__(self):
        self.identification = PluginIdentification(name="ExamplePlugin", version="0.0.2")
        self.configuration = PluginConfiguration(match_patterns=["[0-9]{5,6}"],
                                                 match_only_nearest_to_mouse=True)

    def get_identification(self) -> PluginIdentification:
        return self.identification

    def set_plugin_configuration(self, configuration: str = "", config_file: pathlib.Path = None):
        print(f"{self.identification.name} configuration:\n{configuration}")
        print(f"{self.identification.name} config_file:\n{config_file}")

    def get_configuration(self) -> PluginConfiguration:
        return self.configuration

    def get_translation(self, text, match_pattern: str, match_pattern_index: int) -> str:
        if text == "123456":
            return f"Cool translation for \"{text}\" " \
                   f"[match_pattern={match_pattern}, match_pattern_index={match_pattern_index}]"
        else:
            return f"{self.identification.name} got the text \"{text}\" but didn't find translation " \
                   f"[match_pattern={match_pattern}, match_pattern_index={match_pattern_index}]; "
