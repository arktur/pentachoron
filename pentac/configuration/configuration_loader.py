import shutil
import pathlib

from .configuration import PentacConfig, Plugins


class ConfigurationLoader:

    def __init__(self):
        self.configuration: PentacConfig = None
        self.config_template = pathlib.Path(__file__).parent.joinpath("config_template.yaml")
        self.used_config_file: pathlib.Path = None

    def load_from_file(self, config_file: pathlib.Path, auto_create: bool = True,
                       config_file_template: pathlib.Path = None):

        if config_file_template is not None:
            self.config_template = config_file_template

        self.used_config_file = config_file
        if self.used_config_file is None:
            if auto_create:
                self.used_config_file = self.get_config_file_path()
            else:
                self.used_config_file = self.config_template
        if not self.used_config_file.exists():
            self.create_config_file(self.used_config_file)

        self.configuration = PentacConfig.from_file(self.used_config_file)

    def get_used_config_file(self):
        return self.used_config_file

    def create_config_file(self, config_file: pathlib.Path):
        if not config_file.parent.exists():
            config_file.parent.mkdir(parents=True)
        shutil.copyfile(self.config_template, config_file)

    @staticmethod
    def get_config_file_path() -> pathlib.Path:
        return pathlib.Path.home().joinpath(".pentac/config.yaml")

    def overwrite_settings(self,
                           tesseract_executable_path: pathlib.Path = None,
                           #plugin_modules: List[str],
                           #plugin_module_paths: Dict[str, str] = {},
                           time_after_mouse_stop_s: float = None,
                           theme_name: str = None,
                           theme_transparency_percent: float = None,
                           tray_color: str = None,
                           close_on_mouse_leave: bool = None,
                           follow_mouse_position: bool = None):
        if tesseract_executable_path is not None:
            self.configuration.requirements.tesseract_executable_path = tesseract_executable_path
        if time_after_mouse_stop_s is not None:
            self.configuration.behavior.time_after_mouse_stop_s = time_after_mouse_stop_s
        if theme_name is not None:
            self.configuration.ui.theme_name = theme_name
        if theme_transparency_percent is not None:
            self.configuration.ui.theme_transparency_percent = theme_transparency_percent
        if tray_color is not None:
            self.configuration.tray.icon_color = tray_color
        if close_on_mouse_leave is not None:
            self.configuration.ui.close_on_mouse_leave = close_on_mouse_leave
        if follow_mouse_position is not None:
            self.configuration.ui.follow_mouse_position = follow_mouse_position

    def overwrite_plugins(self, plugins: Plugins = None):
        if plugins is not None:
            self.configuration.plugins = plugins

    def add_plugins(self, additional_plugins: Plugins = None):
        if additional_plugins is not None:
            self.configuration.plugins.extend(additional_plugins)

    def get_configuration(self) -> PentacConfig:
        return self.configuration


def main():
    loader = ConfigurationLoader()
    print(loader.get_config_file_path())


if __name__ == '__main__':
    main()
