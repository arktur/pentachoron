import pathlib
from typing import Union, Hashable, Any, List, Dict

import yaml
import platform
from shutil import which


class ConfigurationObject:

    depth: int = -1
    indentation: str = "    "

    def __repr__(self):

        ConfigurationObject.depth += 1
        indent = ConfigurationObject.depth * ConfigurationObject.indentation

        representation = "\n"
        for item, value in self.__dict__.items():
            representation += f"{indent}{item}: {value}\n"

        ConfigurationObject.depth -= 1
        return representation


class PentacConfig(ConfigurationObject):

    def __init__(self):
        self.requirements: Requirements = None
        self.ui: Ui = None
        self.tray: Tray = None
        self.behavior: Behavior = None
        self.plugins: Plugins = None

    @classmethod
    def from_file(cls, file: pathlib.Path):
        config = cls()
        parsed_config = config.load_config(file)
        config._create_attributes(parsed_config)
        return config

    @staticmethod
    def load_config(config_file: pathlib.Path):
        with open(config_file, 'r') as file:
            return yaml.safe_load(file)

    def _create_attributes(self, parsed_config):
        for key in parsed_config:
            if key == "requirements":
                self.__setattr__(key, Requirements(parsed_config[key]))
            if key == "ui":
                self.__setattr__(key, Ui(parsed_config[key]))
            if key == "tray":
                self.__setattr__(key, Tray(parsed_config[key]))
            elif key == "behavior":
                self.__setattr__(key, Behavior(parsed_config[key]))
            elif key == "plugins":
                self.__setattr__(key, Plugins(parsed_config[key]))


class Requirements(ConfigurationObject):

    tesseract_executable_name = "tesseract"

    def __init__(self, requirements_option):
        self.tesseract_executable_path: pathlib.Path = None

        for option in requirements_option:
            value = requirements_option[option]

            if option == 'tesseract_executable_path':
                value = self._get_tesseract_executable_path(setting=value)

            self.__setattr__(option, value)

    def _get_tesseract_executable_path(self, setting) -> pathlib.Path:
        tesseract_path: pathlib.Path = None

        if "auto" == setting:
            tesseract_path = self._auto_detect_tesseract_executable()
        else:
            tesseract_path = pathlib.Path(setting)
            if not tesseract_path.exists():
                tesseract_path = self._auto_detect_tesseract_executable()

        if tesseract_path is not None and \
                tesseract_path.exists() and \
                tesseract_path.stem == Requirements.tesseract_executable_name:
            return tesseract_path

        return None

    @staticmethod
    def _auto_detect_tesseract_executable():
        tesseract_path: pathlib.Path() = None
        path = which(Requirements.tesseract_executable_name)
        if path is not None:
            tesseract_path = pathlib.Path(path)

        if tesseract_path is None or not tesseract_path.exists():
            default_paths = []
            if "Windows" == platform.system():
                default_paths = [
                    r"C:\Program Files\Tesseract-OCR\tesseract.exe",
                    r"C:\Program Files (x86)\Tesseract-OCR\tesseract.exe",
                ]
            for path in default_paths:
                tesseract_path = pathlib.Path(path)
                if tesseract_path.exists():
                    break

        if tesseract_path is not None and tesseract_path.exists():
            return tesseract_path
        else:
            return None

    def __getitem__(self, item):
        return self.__dict__[item]


class Ui(ConfigurationObject):

    def __init__(self, ui_options):
        self.theme_name: str = None
        self.theme_transparency_percent: float = None
        self.close_on_mouse_leave: bool = None
        self.follow_mouse_position: bool = None

        for option in ui_options:
            value = ui_options[option]
            self.__setattr__(option, value)

    def __getitem__(self, item):
        return self.__dict__[item]


class Tray(ConfigurationObject):

    def __init__(self, tray_options):
        self.icon_color: str = None

        for option in tray_options:
            value = tray_options[option]
            self.__setattr__(option, value)

    def __getitem__(self, item):
        return self.__dict__[item]


class Behavior(ConfigurationObject):

    def __init__(self, behavior_options):
        self.time_after_mouse_stop_s: float = None

        for option in behavior_options:
            value = behavior_options[option]
            self.__setattr__(option, value)

    def __getitem__(self, item):
        return self.__dict__[item]


class Plugin(ConfigurationObject):

    def __init__(self, module: str = None, from_file: str = None,
                 configuration: Union[Dict[Hashable, Any], List[Any], None] = None, configuration_file: str = None,
                 plugin_yaml_config: str = None):
        """
        Keyword arguments:
        configuration -- a parsed yaml configuration returned e.g. by yaml.safe_load(text).
        """
        self.module: str = module
        self.from_file: str = from_file
        self.configuration: Union[Dict[Hashable, Any], List[Any], None] = configuration
        self.configuration_file: str = configuration_file

        if plugin_yaml_config is not None:
            for attribute in plugin_yaml_config:
                value = plugin_yaml_config[attribute]
                self.__setattr__(attribute, value)


class Plugins(list, ConfigurationObject):
    def __init__(self, plugins):
        super().__init__()
        for plugin in plugins:
            self.append(Plugin(plugin_yaml_config=plugin))


def main():
    default_config_file = pathlib.Path(__file__).parent.joinpath("config_template.yaml")
    config = PentacConfig.from_file(file=default_config_file)
    print(config)


if __name__ == '__main__':
    main()
