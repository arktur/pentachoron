# Pentachoron (pentac)

![pentachoron animated][pentachoron animated]
[^1]

[[_TOC_]]

## What is it?

Pentachoron is a small tool that allows translating text near your mouse pointer and display the result in an overlay also appearing near your mouse pointer.

It allows an automatic workflow for general information transformation purposes that makes your life easier in your daily work.

![pentachoron screencast][pentachoron screencast]

It is implemented in python and it is cross platform compatible (tested on Arch based Linux distribution and Windows 10).

The following plugins are included:

- [LinkView](#linkview)

## What does this name mean?

A pentachoron is a four dimensional pyramid (see [5-cell - Wikipedia](https://en.wikipedia.org/wiki/5-cell)). The name was chosen because the physical background is cool and the pentachorons information transformation is as magically done as a four dimensional pyramids projection into the three dimensional space seems to look for a three dimensional human being.

## Information transformation

In general this tool only implements the general workflow for the information transformation:

1. Wait until the mouse stops moving

2. Make a screenshot of the area on the left top of the mouse pointer 

3. Detect text from the screenshot via tesseract ocr

4. Translate the detected text via plugins

5. Show all translations in an on-screen-display on the right top of the mouse pointer

## Usage

### Installation

You can simply 

1. Download and install tesseract from https://tesseract-ocr.github.io/tessdoc/Downloads or install via your distribution. I used tesseract 4.1.1 on an arch based distribution and tesseract 5.0.0 for Windows ([tesseract binaries for windows](https://github.com/UB-Mannheim/tesseract/wiki)).
   Make sure that tesseract executable is in your PATH or if you have windows, make sure to install it to the default location `C:\Program Files\Tesseract-OCR\tesseract.exe`. This is also automatically detected. If pentachoron cannot determine where the executable is, is shows this via the shell output
   
   ```
   Requirement tesseract_executable_path was not found. Try the following steps:
    1. Download and install tesseract from https://tesseract-ocr.github.io/tessdoc/Downloads or install via your distribution
    2. Add the executable to your PATH (if not done automatically) or configure the full path to the executable tesseract.exe inside your config file C:\Users\SESA103057\.pentac\config.yaml, instead of using auto detection.
   ```
   
   You can fix this by simply configuring the `tesseract_executable_path` option with the absolute path to the `tesseract` executable (inside the `requirements` configuration section).

2. create a new poetry package
   
   ```
   poetry new my_pentac_integration
   ```

3. make the package ready
   
   ```
   cd my_pentac_integration
   poetry install
   ```

4. Set compatibility to the level required by pentachoron (due to PySide6 restrictions). To do this, enter the following python dependencies:
   
   ```
    [tool.poetry.dependencies]
    python = ">=3.7, <3.11"
   ```

5. Install pentachoron into the package
   
   ```
       poetry add git+https://gitlab.com/arktur/pentachoron
   ```

### Running the `pentac` application

```
poetry run pentac
```

Via the tray icon, you can close the application and see all active configurations. 

![system tray icon][tray icon]

The configuration menu only views the determined configuration and does not change it.

![tray menu][tray menu]

### Configuration

The following code snippet shows the default configuration which is saved inside file [configuration template][configuration template].

```yaml
requirements:
  tesseract_executable_path: auto     # auto: Pentachoron will try to find the executable automatically. If this does not work, enter a full path to the executable here.

ui:
  theme_name: dracula                 # Values: dracula, clean_dark
  theme_transparency_percent: 20.0    # transparency in % as float
  close_on_mouse_leave: True          # True: Close the overlay as soon as mouse leaves it. False: Overlay will not close.
  follow_mouse_position: True         # True: Overlay is always shown on right top of mouse position.

tray:
  icon_color: green                   # Values: deep_blue, green, mint, orange, pink, purple, red, yellow, yellow_green

behavior:
  time_after_mouse_stop_s: 1.0        # Time in seconds as float after that the text detection is started.

plugins:
  - module: pentac.plugins.example.example_plugin   # Module name
    configuration:                                  # pentac: optional / plugin dependent: Yaml configuration that is given directly to the plugin.
      setting_1: cool setting 1 value
      setting_2: cool setting 2 value
    configuration_file: test_configuration.yaml     # Optional. File path that is given directly to the plugin.
#  - module: pentac.plugins.link_view.plugin
#    configuration:                                  # pentac: optional / plugin dependent: Yaml configuration that is given directly to the plugin.
#      - match_patterns:                             # List of all patterns that should be transformed to links
#          - "[#]{1}[0-9]{1,4}"
#        links:                                      # List of all links that can be displayed
#          - for_match_pattern: "[#]{1}[0-9]{1,4}"   # Optional: Link should only be displayed for specific match pattern
#            text_replacements:                      # Optional: replace stuff in source text before going on with link creation.
#              - replace: "#"
#                with: ""
#            link: https://gitlab.com/arktur/pentachoron/-/issues/[[text]]   # Link to create. [[text]] can be used to insert the text including all configured replacements.
```

As soon as the application runs the first time, it creates a new configuration file `~/.pentac/config.yaml` from this template. If you want to reset pentachoron to this default configuration, you can simply delete the file inside your home directory and let it creates a new one at the next start.

### Use cases

You can think of many use cases for this tool. The initial use case for me was, that I needed to translate company internal employee numbers to their names on the fly.

Mentionable ideas for use cases:

- Showing links to tickets (e.g. on detection of ticket numbers, show the link to your ticket system) - already implemented in [LinkView plugin](#linkview)

- Translating words between languages

- ....

## Plugins

### LinkView

The LinkView plugin supports the creation of html links. The plugin is fully configurable, so you can simply make it fit to your use case within your pentachorons config file. The following example will find all strings that start with a `#` and have then 1 to 4 digits and display a link to the pentachorons Gitlab issues site.

```yaml
requirements:
[...]
ui:
[...]
tray:
[...]
behavior:
[...]

plugins:
  - module: pentac.plugins.link_view.plugin
    configuration:
      - match_patterns:                             # List of all patterns that should be transformed to links
          - "[#]{1}[0-9]{1,4}"
        links:                                      # List of all links that can be displayed
          - for_match_pattern: "[#]{1}[0-9]{1,4}"   # Optional: Link should only be displayed for specific match pattern
            text_replacements:                      # Optional: replace stuff in source text before going on with link creation.
              - replace: "#"
                with: ""
            link: https://gitlab.com/arktur/pentachoron/-/issues/[[text]]   # Link to create. [[text]] can be used to insert the text including all configured replacements.
```

The link view plugin can be loaded by configuring the plugin module `pentac.plugins.link_view.plugin`. It supports the `configuration` option which holds the complete configuration for the plugin. The `configuration_file` option is not supported and will lead to an `NotImpelemenedError` exception.

The plugin can be configured for multiple link types by adding more than one list item under `configuration`.

```yaml
plugins:
  - module: pentac.plugins.link_view.plugin
    configuration:
      - match_patterns:                             # List of all patterns that should be transformed to links
          - "[#]{1}[0-9]{1,4}"
        links:                                      # List of all links that can be displayed
          - link: https://gitlab.com/arktur/pentachoron/-/issues/[[text]]   # Link to create. [[text]] can be used to insert the text including all configured replacements.
      - match_patterns:
          - "MyProject-[0-9]{1,6}"
        links:
          - link: https://my.ticket.system.pirate/issues/[[text]]
      - match_patterns:
        [...]
      - match_patterns:
        [...]
      - match_patterns:
        [...]
```

Each link type has a list of `match_patterns` that should match this link type and a `links` list which tells the systems which links to display. Each link can use the `[[text]]` placeholder to insert the matched text (in the example this is the ticket number). 

The matched text can be preprocessed by multiple `text_replacements`. This can be used e.g. to change the content that will be filled in the `[[text]]` placeholder first. This is e.g. necessary to match `#0815` to a Gitlab issue which needs only the number in the URL.  This example can simply be solved by replacing `"#"` with `""` (see example above). This is optional.

The system can generate multiple links per link type. Each created link can be configured to just being displayed for specific match patterns by using the `for_match_pattern` option. This is optional.

## Plugin system

In order to make the tool extensible with any use case, it implements a plugin system. The system relevant plugin manager and interfaces parts are implemented inside the package `pentac.plugin`.

A plugin can either be a python package that is installed inside the system, or a single python file (if it does not need required python packages that are possibly not installed inside the system).

### How to implement a Plugin

A plugin can be implemented easily by using [this template](https://gitlab.com/arktur/pentachoron-plugin-template/) or by doing the following steps:

1. create a python file with the following content somewhere in your file system, e.g. as `~./tmp/example_plugin.py`
   
   ```python
    import pathlib
   
    from pentac.plugin import PluginManager, PluginInterface, PluginIdentification, PluginConfiguration
   
    @PluginManager.plugin
    class ExamplePlugin(PluginInterface):
   
    def __init__(self):
        self.identification = PluginIdentification(name="ExamplePlugin", version="0.0.2")
        self.configuration = PluginConfiguration(match_patterns=["[0-9]{5,6}"],
                                                 match_only_nearest_to_mouse=True)
   
    def get_identification(self) -> PluginIdentification:
        return self.identification
   
    def set_plugin_configuration(self, configuration: str = "", config_file: pathlib.Path = None):
        print(f"{self.identification.name} configuration:\n{configuration}")
        print(f"{self.identification.name} config_file:\n{config_file}")
   
    def get_configuration(self) -> PluginConfiguration:
        return self.configuration
   
    def get_translation(self, text, match_pattern: str, match_pattern_index: int) -> str:
        if text == "123456":
            return f"Cool translation for \"{text}\" " \
                   f"[match_pattern={match_pattern}, match_pattern_index={match_pattern_index}]"
        else:
            return f"{self.identification.name} got the text \"{text}\" but didn't find translation " \
                   f"[match_pattern={match_pattern}, match_pattern_index={match_pattern_index}]; "
   ```

2. configure the plugin to load inside pentachoron application by configuring it in the `plugins` section inside the `config.yaml`:
   
   ```yaml
   [...]
   
   plugins:
     - module: pentac.plugins.my_example_plugin.example_plugin   # Module name
       from_file: full/path/to/your/example.plugin.py            # Optional. Loads the module from a .py file instead of importing the module path.
   ```
   
   This is the easiest configuration. The `from_file` option is needed, if the plugin package is not installed inside the system or your virtual env, but is only available as a single file inside your file system.

### Understanding plugins

The plugin interface is really simple. The PluginManager simply does the following

1. At startup, it iterates through all plugin configurations and loads each module by either simple importing the namespace (if `from_file` is not given) or imports a specific file specified in `from_file` and makes it available under the configured `module` package path.

2. All classes that have the `@PluginManager.plugin` decorator are automatically instantiated once.

3. Each instance is asked for its identification data via 
   
   ```python
    def get_identification(self) -> PluginIdentification:
        ...
   ```

4. The plugins `set_plugin_configuration` is called and the settings are transferred to the plugin
   
   ```python
    def set_plugin_configuration(self, configuration: str = "", config_file: pathlib.Path = None):
        ...
   ```
   
   The plugin does not have to do anything here, but it can be used to retrieve yaml configurations or configuration file paths for this plugin from the pentachorons `config.yaml` file.

5. The plugin is asked for its configuration, this means which text patterns does it want to receive and does it only need the last occurrence of this patterns:
   
   ```python
    def get_configuration(self) -> PluginConfiguration:
        ...
   ```

6. After this startup procedure pentachoron tries to find matches for the patterns returned in `get_configuration` and sends the result of the matching to the plugin via
   
   ```python
    def get_translation(self, text, match_pattern: str, match_pattern_index: int) -> str:
        ...
   ```
   
   in order to get a translation from the plugin.
   The plugins return value is displayed as markdown, so you can return simple text, html content or markdown content.

The example plugin above simply tells the system, that it can translate any number consisting of the regex pattern `[0-9]{5,6}` (number with 5 to 6 digits) and translates it in some way.

### Guidelines

It is recommended to use the namespace "pentac.plugins.*" for new plugins. This namespace is reserved for plugins.

![directory structure plugins]

## Future improvements

- [ ] Change mechanism of preprocessing the images before tesseract detection is started: At the moment the screenshot mechanism detects the dpi resolution of the monitor where the shot is taken and the resizing is done to a specified overall dpi.
  In the future it could detect text lines in the screenshot (with cv2) and their size, resize each text line to the optimal text size (see tesseract documentation) and push then the text lines to tesseract detection.

- [ ] Plugin mechanism:
  
  - [ ] Let the plugin decide how big the screenshot must be
  
  - [ ] Let the plugin decide per match pattern, which language should be used for text detection (tesseract language configuration support) and add default language setting to config.yaml

## License

see [LICENSE file][license file]. 

## Certification

![official certificate][official certificate]

## Debugging

For run-time debugging, you can use the following `DebugFlags` to enable more output on the shell.

```
class DebugFlags:
    print_text_translation = False
    print_detection_profiling = False
```

They can be activated by importing `DebugFlags` and just setting the flags to true before starting the `PentacApp`.

```python
from pentac.application import PentacApp
from pentac.application import DebugFlags

def main():

    DebugFlags.print_text_translation = True
    DebugFlags.print_detection_profiling = True

    app = PentacApp()
    app.execute()

if __name__ == '__main__':
    main()
```

- The `DebugFlags.print_text_translation` flag enables the shell output of all text that is detected via tesseract ocr and the feedback of all plugins.
- The `DebugFlags.print_detection_profiling` enables the `cProfile.runctx()` output of the complete detection algorithm (screen shooting, text detection, text transformation, result viewing).

# References

## Images

- [pentachoron animated]: ./documentation/pictures/5-cell.gif "Pentachoron animated"
- [pentachoron screencast]: ./documentation/pictures/pentachoron-example-plugin-screencast.gif "Pentachoron screencast of example plugin"
- [directory structure plugins]: ./documentation/pictures/plugins-directory-structure.jpg "Image that shows the pentac.plugins.* namespace"
- [tray menu]: ./documentation/pictures/tray_menu.png "Pentachoron system tray menu"
- [tray icon]: ./documentation/pictures/tray_icon.png "Pentachoron system tray icon"
- [official certificate]: ./documentation/pictures/badge_works-on-my-machine_certificate.png "Official certification"

## Other documents

- [license file]: ./LICENSE
- [configuration template]: ./pentac/configuration/config_template.yaml

## Footnotes

[^1]:  Taken from [File:5-cell.gif - Wikipedia](https://en.wikipedia.org/wiki/File:5-cell.gif)
